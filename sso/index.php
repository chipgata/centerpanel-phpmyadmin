<?php
declare(strict_types=1);

require 'vendor/autoload.php';
use \Firebase\JWT\JWT;

/**
 * Single signon for phpMyAdmin
 *
 * This is just example how to use session based single signon with
 * phpMyAdmin, it is not intended to be perfect code and look, only
 * shows how you can integrate this functionality in your application.
 */

/* Use cookies for session */
ini_set('session.use_cookies', 'true');
/* Change this to true if using phpMyAdmin over https */
$secure_cookie = false;
/* Need to have cookie visible from parent directory */
session_set_cookie_params(0, '/', '', $secure_cookie, true);
/* Create signon session */
$session_name = 'CenterPanelSignonSession';
session_name($session_name);
// Uncomment and change the following line to match your $cfg['SessionSavePath']
session_save_path('../sessions');
@session_start();

if (isset($_SESSION['PMA_single_signon_error_message'])) {
  echo $_SESSION['PMA_single_signon_error_message'];
  unset($_SESSION['PMA_single_signon_error_message']);
  @session_write_close();
  return;
}

$jwt = $_GET['token'];
$key = getenv('PMA_SECRET_KEY') != "" ? getenv('PMA_SECRET_KEY') : '123456';
if(!$jwt){
    echo "Login failed, please try again";
    die();
}
$user = (array)JWT::decode($jwt, $key, array('HS256'));
if(!$user){
   echo 'Login failed, please try again';
   die();
}
/* Was data posted? */
if (isset($user)) {
    /* Store there credentials */
    $_SESSION['PMA_single_signon_user'] = $user['username'];
    $_SESSION['PMA_single_signon_password'] = $user['password'];
    $_SESSION['PMA_single_signon_host'] = $user['host'];
    $_SESSION['PMA_single_signon_port'] = $user['port'];
    /* Update another field of server configuration */
    $_SESSION['PMA_single_signon_cfgupdate'] = ['verbose' => 'Signon test'];
    $_SESSION['PMA_single_signon_HMAC_secret'] = hash('sha1', uniqid(strval(rand()), true));
    $id = session_id();
    /* Close that session */
    @session_write_close();
    /* Redirect to phpMyAdmin (should use absolute URL here!) */
    header('Location: ../index.php');
}